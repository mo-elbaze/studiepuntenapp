package nl.elbaze.studiepuntenapp.auth;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RequestInterface {

    @POST("auth/")
    Call<ServerResponse> operation(@Body ServerRequest request);

}
