package nl.elbaze.studiepuntenapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

/**
 * @author Mohamed El Baze
 * @version 0.1
 * @date 1/29/17
 */
public class SpecialisatieAdapter extends ArrayAdapter<Button> {
    Context context;

    public SpecialisatieAdapter(Context context, int resource) {
        super(context, resource);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return convertView;
    }
}
