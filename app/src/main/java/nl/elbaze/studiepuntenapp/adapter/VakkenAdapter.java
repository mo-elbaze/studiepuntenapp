package nl.elbaze.studiepuntenapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import nl.elbaze.studiepuntenapp.R;
import nl.elbaze.studiepuntenapp.model.Vakken;

import java.util.List;

/**
 * @author Mohamed El Baze
 * @version 0.1
 * @date 1/15/17
 */
public class VakkenAdapter extends ArrayAdapter<Vakken> {
    Context context;

    private static class ViewHolder {
        TextView title;
        TextView name;
        ImageView imageView;
    }

    public VakkenAdapter(Context context, List<Vakken> vakken) {
        super(context, 0, vakken);
        this.context = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        final Vakken vakken = getItem(position);

        ViewHolder viewHolder; //check for cache
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_vakken, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Populate the data into the template view using the vakken object
        viewHolder.name.setText(vakken.getSpecialisatie()+" vak:"+vakken.getCode());

        /*
        If your item has image you could load it dynamicaly with Picasso
        if(vakken.getImagePath()!=""){
            Picasso.with(getContext())
                    .load(vakken.getImagePath())
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(viewHolder.imageView);
        }
        */
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("Vak selected", vakken.toString());
//                Intent intent = new Intent(context, VakkenDetail.class);
//                Log.d("ID", vakken.getCode()+"");
//                intent.putExtra("module_id",vakken.getCode());
//                context.startActivity(intent);
//            }
//        });
        // Return the completed view to render on screen
        return convertView;

    }
}
