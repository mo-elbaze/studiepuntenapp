package nl.elbaze.studiepuntenapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import nl.elbaze.studiepuntenapp.R;
import nl.elbaze.studiepuntenapp.model.Gebruikers;

import java.util.List;

/**
 * @author Mohamed El Baze
 * @version 0.1
 * @date 1/15/17
 */
public class GebruikersAdapter extends ArrayAdapter<Gebruikers> {

        private static class ViewHolder {
            TextView title;
            TextView name;
            ImageView imageView;
        }

    public GebruikersAdapter(Context context, List<Gebruikers> gebruikerss) {
            super(context, 0, gebruikerss);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            // Get the data item for this position
            Gebruikers gebruikers = getItem(position);

            ViewHolder viewHolder; //check for cache
            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.list_item_gebruikers, parent, false);
                viewHolder.name = (TextView) convertView.findViewById(R.id.name);
                viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageView);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            // Populate the data into the template view using the gebruikers object
            viewHolder.name.setText(gebruikers.getVoornaam()+" "+gebruikers.getAchternaam());

        /*
        If your item has image you could load it dynamicaly with Picasso
        if(gebruikers.getImagePath()!=""){
            Picasso.with(getContext())
                    .load(gebruikers.getImagePath())
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(viewHolder.imageView);
        }
        */

            // Return the completed view to render on screen
            return convertView;

        }
    }
