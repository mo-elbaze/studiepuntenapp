package nl.elbaze.studiepuntenapp.services;

import nl.elbaze.studiepuntenapp.model.GebruikersResults;
import nl.elbaze.studiepuntenapp.model.GebruikersVakken;
import nl.elbaze.studiepuntenapp.model.GebruikersVakkenResults;
import nl.elbaze.studiepuntenapp.model.VakkenResults;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * @author Mohamed El Baze
 * @version 0.1
 * @date 1/15/17
 */
public interface APIPlug {

    /*
    These methods defines our API endpoints.
    All REST methods such as GET,POST,UPDATE,DELETE can be stated in here.
    */
    @GET("gebruikers?transform=1")
    Call<GebruikersResults> getGebruikers(@Query(encoded = true,value = "filter") String studentmail);

    @GET("vakken?transform=1")
    Call<VakkenResults> getVakken(@Query(encoded = true,value = "filter")String specialisatie);

    @GET("gebruiker_has_vakken?transform=1")
    Call<GebruikersVakkenResults> getGebruikersVakken(@Query(encoded = true,value = "filter") String studentmail);

    @FormUrlEncoded
    @POST("gebruiker_has_vakken?transform=1")
    Call<GebruikersVakken> setGebruikersVakken(@Field("email") String email, @Field("code") String code,
                                               @Field("cijfer") double cijfer);

}