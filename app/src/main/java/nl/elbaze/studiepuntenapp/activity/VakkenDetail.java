package nl.elbaze.studiepuntenapp.activity;
/**
 * @author Idriss El Baze, Mohamed EL Baze
 * @version 0.1
 * @date 1/17/17
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import nl.elbaze.studiepuntenapp.R;
import nl.elbaze.studiepuntenapp.model.GebruikersVakken;
import nl.elbaze.studiepuntenapp.services.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;

public class VakkenDetail extends AppCompatActivity {

    String Specialisatie ="";
    String email = "";
    String Code = "";
    String Cijfers ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vakken_detail);

        Bundle extras = getIntent().getExtras();
        Specialisatie = extras.getString("Specialisatie");
        String Specialisatievak = extras.getString("Specialisatievak");
        String EC = extras.getString("EC");
        Code = extras.getString("CODE");
        String SBU = extras.getString("SBU");
        Cijfers = extras.getString("Cijfer");
        email = extras.getString("Email");
        System.out.println("----------->>> DETAILS EMAAIL");

        TextView specialisatie  = (TextView) findViewById(R.id.specialisatiedetailnaam);
        specialisatie.setText(Specialisatievak);
        TextView ec = (TextView) findViewById(R.id.ects);
        ec.setText(EC);
        TextView sbu = (TextView) findViewById(R.id.sbu);
        sbu.setText(SBU);
        TextView cijfer = (TextView) findViewById(R.id.scijfer);
        cijfer.setText(Cijfers);





    }

    public void Addscijfer(View view) throws IOException {
        TextView cijfer = (TextView) findViewById(R.id.scijfer);
        TextView nummer = (TextView) findViewById(R.id.edit_message);
//        cijfer.setText(Cijfers);


        Call<GebruikersVakken> call = ApiClient.get().setGebruikersVakken(email,Code,
                Double.parseDouble(nummer.getText().toString()));
        call.enqueue(new Callback<GebruikersVakken>() {
            @Override
            public void onResponse(Call<GebruikersVakken> call, Response<GebruikersVakken> response) {
                Log.d("APIPlug", "Error Occured: " + response);
            }

            @Override
            public void onFailure(Call<GebruikersVakken> call, Throwable t) {
                Log.d("APIPlug", "Error Occured: " + t.getMessage());
            }
        });
//        nummer.setText("");
    }

}
