package nl.elbaze.studiepuntenapp.activity.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import nl.elbaze.studiepuntenapp.R;
import nl.elbaze.studiepuntenapp.adapter.GebruikersVakkenAdapter;
import nl.elbaze.studiepuntenapp.auth.Constants;
import nl.elbaze.studiepuntenapp.model.GebruikersVakken;
import nl.elbaze.studiepuntenapp.model.GebruikersVakkenResults;
import nl.elbaze.studiepuntenapp.services.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

/**
 * @author Mohamed El Baze
 * @version 0.1
 * @date 1/15/17
 */
public class GebruikersVakkenFragment extends android.app.Fragment {

    private List<GebruikersVakken> gebruikersVakken;
    private ListView listView;
    private SharedPreferences pref;

    public GebruikersVakkenFragment() {
        // Required empty public constructor
    }

    /**
     * Use this fgebruikersVakkeny method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FeedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GebruikersVakkenFragment newInstance() {
        GebruikersVakkenFragment fragment = new GebruikersVakkenFragment();
        Bundle args = new Bundle();
        //If your fragment needs params add them in here
        //args.putString(ARG_PARAM1, param1);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //If your fragment needs params add them in here
            //mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gebruikersvakken, container, false);

        listView = (ListView) view.findViewById(R.id.listView);

        getGebruikersVakkenList();

        return view;
    }

    private void getGebruikersVakkenList() {
        final ProgressDialog loading = ProgressDialog.show(getActivity(), getContext().getString(R.string.loading_title), getContext().getString(R.string.loading_please_wait), false, false);
        pref = getActivity().getPreferences(0);
        //If you have detail activity about your object define in here
        String email = pref.getString(Constants.EMAIL,"");
        Call<GebruikersVakkenResults> call = ApiClient.get().getGebruikersVakken("email,eq," +email);

        call.enqueue(new Callback<GebruikersVakkenResults>() {
            @Override
            public void onFailure(Call<GebruikersVakkenResults> call, Throwable t) {
                Log.d("APIPlug", "Error Occured: " + t.getMessage());

                loading.dismiss();
            }

            @Override
            public void onResponse(Call<GebruikersVakkenResults> call, Response<GebruikersVakkenResults> response) {
                Log.d("APIPlug", "Successfully response fetched");

                loading.dismiss();

                gebruikersVakken = response.body().results;

                if (gebruikersVakken.size() > 0) {
                    showList();
                } else {
                    Log.d("APIPlug", "No item found");
                }
            }
        });
    }

    //Our method to show list
    private void showList() {
        Log.d("APIPlug", "Show List");

        GebruikersVakkenAdapter adapter = new GebruikersVakkenAdapter(getActivity(), gebruikersVakken);
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GebruikersVakken clickedObj = (GebruikersVakken) parent.getItemAtPosition(position);

                //If you have detail activity about your object define in here
            /*
            Intent detail = new Intent(getContext(), GebruikersVakkenDetail.class);
            detail.putExtra("gebruikersVakkenObject", clickedObj);
            startActivity(detail);
            */
            }
        });

    }
}