package nl.elbaze.studiepuntenapp.activity.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import nl.elbaze.studiepuntenapp.R;
import nl.elbaze.studiepuntenapp.activity.VakkenDetail;
import nl.elbaze.studiepuntenapp.adapter.VakkenAdapter;
import nl.elbaze.studiepuntenapp.auth.Constants;
import nl.elbaze.studiepuntenapp.model.GebruikersVakken;
import nl.elbaze.studiepuntenapp.model.Vakken;
import nl.elbaze.studiepuntenapp.model.VakkenResults;
import nl.elbaze.studiepuntenapp.services.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mohamed El Baze
 * @version 0.1
 * @date 1/15/17
 */
public class VakkenFragment extends android.app.Fragment {


    private List<Vakken> vakken;
    private List<GebruikersVakken> gebruikersVakken;
    private ListView listView;
    private SharedPreferences pref;



    public VakkenFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FeedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VakkenFragment newInstance() {
        VakkenFragment fragment = new VakkenFragment();
        Bundle args = new Bundle();
        //If your fragment needs params add them in here
        //args.putString(ARG_PARAM1, param1);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vakken, container, false);

        listView = (ListView) view.findViewById(R.id.listView);
        pref = getActivity().getPreferences(0);

        getVakkenList();


        return view;
    }
    
    private void getVakkenList() {
        final ProgressDialog loading = ProgressDialog.show(getActivity(),getContext().getString(R.string.loading_title),getContext().getString(R.string.loading_please_wait),false,false);
        pref = getActivity().getPreferences(0);

        String specialisatie = pref.getString(Constants.SPECIALISATIEKEUZE,"");
        Call<VakkenResults> call = ApiClient.get().getVakken("specialisatie,eq," +specialisatie);

        call.enqueue(new Callback<VakkenResults>() {
            @Override
            public void onFailure(Call<VakkenResults> call, Throwable t) {
                Log.d("APIPlug", "Error Occured: " + t.getMessage());

                loading.dismiss();
            }

            @Override
            public void onResponse(Call<VakkenResults> call, Response<VakkenResults> response) {
                Log.d("APIPlug", "Successfully response fetched" );

                loading.dismiss();

                vakken=response.body().results;

                if(vakken.size()>0) {
                    showList();
                }else{
                    Log.d("APIPlug", "No item found");
                }
            }
        });
    }
    List<Vakken> filtered = new ArrayList<Vakken>();
    //Our method to show list
    private void showList() {
        filtered.clear();
        for(Vakken article : vakken)
        {
            filtered.add(article);
        }


        Log.d("APIPlug", "Show List filterd lengte :::: " + filtered.size());
        VakkenAdapter adapter = new VakkenAdapter(getActivity(), filtered);
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Vakken clickedObj = (Vakken) parent.getItemAtPosition(position);
                pref = getActivity().getPreferences(0);
                //If you have detail activity about your object define in here
                String email = pref.getString(Constants.EMAIL,"");
                System.out.println("------------------>>>>EMAIL is ::::" + email);
            Intent detail = new Intent(getContext(), VakkenDetail.class);
                detail.putExtra("Specialisatie", clickedObj.getSpecialisatie());
                detail.putExtra("Specialisatievak", clickedObj.getSpecialisatie() + ": " + clickedObj.getCode());
                detail.putExtra("EC", clickedObj.getEc());
                detail.putExtra("CODE", clickedObj.getCode());
                detail.putExtra("SBU", clickedObj.getSbu());
                detail.putExtra("Cijfer", String.valueOf(clickedObj.getCijfer()));
                System.out.println("---------------------<<<>>>>>>CIJFER:" + clickedObj.getCijfer());
                detail.putExtra("Email",email);

            startActivity(detail);

            }});

    }

}