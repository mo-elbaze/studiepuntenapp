package nl.elbaze.studiepuntenapp.activity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import nl.elbaze.studiepuntenapp.R;
import nl.elbaze.studiepuntenapp.activity.fragments.*;
import nl.elbaze.studiepuntenapp.auth.Constants;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        SpecialisatieFragment.OnFragmentInteractionListener {

    private SharedPreferences pref;
    private android.app.Fragment fragment;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private boolean keuze = false;

    private void initFragment(){
        if(pref.getBoolean(Constants.IS_LOGGED_IN,false) &&  keuze == false){
            fragment = new SpecialisatieFragment();
        } else {
            fragment = new LoginFragment();
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_Frame,fragment);
        ft.commit();


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pref = getPreferences(0);
        initFragment();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        android.app.Fragment fragment = new android.app.Fragment();

        if (id == R.id.nav_camera) {
            fragment = (android.app.Fragment) GebruikersFragment.newInstance();
            getSupportActionBar().setTitle("Overzicht");
        } else if (id == R.id.nav_gallery) {
            fragment = (android.app.Fragment) VakkenFragment.newInstance();
            getSupportActionBar().setTitle("Vakken");

        } else if (id == R.id.nav_slideshow) {
            fragment = (android.app.Fragment) GebruikersVakkenFragment.newInstance();
            getSupportActionBar().setTitle("Behaalde Vakken");

        } else if (id == R.id.nav_manage) {
            fragment = new ProfileFragment();
            getSupportActionBar().setTitle("Instellingen");

        } else if (id == R.id.nav_share) {
            shareIt();
        }

        android.app.FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_Frame, fragment).commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void keuzeBDAM(View view) {
        android.app.Fragment fragment = (android.app.Fragment) VakkenFragment.newInstance();
        FragmentTransaction fragmentManager = getFragmentManager().beginTransaction();
        fragmentManager.replace(R.id.fragment_Frame, fragment).commit();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.SPECIALISATIEKEUZE,"BDAM");
        editor.apply();
        //If you have detail activity about your object define in here
        String specialisatie = pref.getString(Constants.SPECIALISATIEKEUZE,"");
        showmenu();
    }

    public void keuzeSE(View view) {
        android.app.Fragment fragment = (android.app.Fragment) VakkenFragment.newInstance();
        FragmentTransaction fragmentManager = getFragmentManager().beginTransaction();
        fragmentManager.replace(R.id.fragment_Frame, fragment).commit();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.SPECIALISATIEKEUZE,"SE");
        editor.apply();
        //If you have detail activity about your object define in here
        String specialisatie = pref.getString(Constants.SPECIALISATIEKEUZE,"");
        showmenu();
    }
    public void keuzeFICT(View view) {
        android.app.Fragment fragment = (android.app.Fragment) VakkenFragment.newInstance();
        FragmentTransaction fragmentManager = getFragmentManager().beginTransaction();
        fragmentManager.replace(R.id.fragment_Frame, fragment).commit();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.SPECIALISATIEKEUZE,"FICT");
        editor.apply();
        //If you have detail activity about your object define in here
        String specialisatie = pref.getString(Constants.SPECIALISATIEKEUZE,"");
        showmenu();
    }
    public void keuzeMEDT(View view) {
        android.app.Fragment fragment = (android.app.Fragment) VakkenFragment.newInstance();
        FragmentTransaction fragmentManager = getFragmentManager().beginTransaction();
        fragmentManager.replace(R.id.fragment_Frame, fragment).commit();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.SPECIALISATIEKEUZE,"MEDT");
        editor.apply();
        //If you have detail activity about your object define in here
        String specialisatie = pref.getString(Constants.SPECIALISATIEKEUZE,"");
        showmenu();
    }


    public void showmenu() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        if(pref.getBoolean(Constants.IS_LOGGED_IN,false) &&  keuze == false) {
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

            setSupportActionBar(toolbar);


            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            navigationView.setNavigationItemSelectedListener(this);

            View navigationMenuView = navigationView.getHeaderView(0);
            TextView viewNaamGebruiker = (TextView) navigationMenuView.findViewById(R.id.navNaamGebruiker);
            TextView viewEmailGebruiker = (TextView) navigationMenuView.findViewById(R.id.navEmailGebruiker);
            viewNaamGebruiker.setText(pref.getString(Constants.NAME,""));
            viewEmailGebruiker.setText(pref.getString(Constants.EMAIL,""));
        }
        else {
            navigationView.setVisibility(0);
        }

    }


    public void shareIt(){

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = getString(R.string.share_text);
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Deel via"));
    }

//    private void requestSubjects() {
//        Type type = new TypeToken<List<Module>>() {
//        }.getType();
//
//        GsonRequest<List<Module>> request = new GsonRequest<List<Module>>("http://demo2223794.mockable.io",
//                type, null, new Response.Listener<List<Module>>() {
//            @Override
//            public void onResponse(List<Module> response) {
//                processRequestSucces(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                processRequestError(error);
//            }
//
//            private void processRequestError(VolleyError error) {
//                System.out.printf("errror: --->>" + error);
//            }
//        });
//        ModuleHelper.getInstance(this).addToRequestQueue(request);
//    }
//
//    private void processRequestSucces(List<Module> subjects) {
//        for (Module cm : subjects) {
//            authList.add(cm);
//            mAdapter = new RecyclerAdapter(MainActivity.this, authList);
//            mListView.setAdapter(mAdapter);
//            mListView.setLayoutManager(new LinearLayoutManager(this));
//        }
//    }
}
