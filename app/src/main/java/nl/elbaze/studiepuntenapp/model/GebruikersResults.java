package nl.elbaze.studiepuntenapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Mohamed El Baze
 * @version 0.1
 * @date 1/15/17
 */
public class GebruikersResults {
    @SerializedName("gebruikers")
    public List<Gebruikers> results;
}
