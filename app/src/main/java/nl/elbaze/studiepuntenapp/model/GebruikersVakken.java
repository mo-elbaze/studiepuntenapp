package nl.elbaze.studiepuntenapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Mohamed El Baze
 * @version 0.1
 * @date 1/15/17
 */
public class GebruikersVakken implements Serializable{
    @SerializedName("email")
    private String email;
    @SerializedName("code")
    private String code;
    @SerializedName("cijfer")
    private double cijfer;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getCijfer() {
        return cijfer;
    }

    public void setCijfer(double cijfer) {
        this.cijfer = cijfer;
    }
}
