package nl.elbaze.studiepuntenapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Mohamed El Baze
 * @version 0.1
 * @date 1/15/17
 */
public class Gebruikers implements Serializable{
    @SerializedName("studentnr")
    private String studentnr;
    @SerializedName("wachtwoord")
    private String wachtwoord;
    @SerializedName("voornaam")
    private String voornaam;
    @SerializedName("achternaam")
    private String achternaam;
    @SerializedName("email")
    private String email;
    private String old_password;
    private String new_password;

    public String getStudentnr() {
        return studentnr;
    }

    public void setStudentnr(String studentnr) {
        this.studentnr = studentnr;
    }

    public String getWachtwoord() {
        return wachtwoord;
    }

    public void setWachtwoord(String wachtwoord) {
        this.wachtwoord = wachtwoord;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }
}
