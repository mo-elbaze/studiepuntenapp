package nl.elbaze.studiepuntenapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author Mohamed El Baze
 * @version 0.1
 * @date 1/15/17
 */
public class VakkenResults  implements Serializable{
    @SerializedName("vakken")
    public List<Vakken> results;
}
