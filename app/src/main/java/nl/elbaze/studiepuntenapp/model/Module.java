package nl.elbaze.studiepuntenapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Mohamed El Baze
 * @version 0.1
 * @date 1/13/17
 */

public class Module  implements Serializable {
    private String Code;
    private String Specialisatie_keuze;
    private String Jaar;
//    private String Periode[];
    @SerializedName("Periode 1")
    private String Periode1;
    @SerializedName("Periode 2")
    private String Periode2;
    @SerializedName("Periode 3")
    private String Periode3;
    @SerializedName("Periode 4")
    private String Periode4;
    private String EC;
    private String SBU;
    @SerializedName("Naam")
    private String Beschrijving;


    public Module() {
    }

    public String getCode() {
        return Code +": "+ Beschrijving;
    }

    public void setCode(String code) {
        this.Code = code;
    }

    public String getSpecialisatie_keuze() {
        return Specialisatie_keuze + ":" + Jaar;
    }

    public void setSpecialisatie_keuze(String specialisatie_keuze) {
        Specialisatie_keuze = specialisatie_keuze;
    }

    public String getJaar() {
        return Jaar;
    }

    public void setJaar(String jaar) {
        Jaar = jaar;
    }

    public String getPeriode1() {
        return Periode1;
    }

    public void setPeriode1(String periode1) {
        Periode1 = periode1;
    }

    public String getPeriode2() {
        return Periode2;
    }

    public void setPeriode2(String periode2) {
        Periode2 = periode2;
    }

    public String getPeriode3() {
        return Periode3;
    }

    public void setPeriode3(String periode3) {
        Periode3 = periode3;
    }

    public String getPeriode4() {
        return Periode4;
    }

    public void setPeriode4(String periode4) {
        Periode4 = periode4;
    }

    public String getEC() {
        return EC;
    }

    public void setEC(String EC) {
        this.EC = EC;
    }

    public String getSBU() {
        return SBU;
    }

    public void setSBU(String SBU) {
        this.SBU = SBU;
    }

    public String getBeschrijving() {
        return Beschrijving;
    }

    public void setBeschrijving(String beschrijving) {
        Beschrijving = beschrijving;
    }
}
